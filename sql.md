# InfocomII

 1- Servidor Base de Datos
    1.1- Se modifica el sources.list, para agregar o eliminar origines de paquetes.
      $sudo nano /etc/apt/sources.list
    despues de main se agrega (contrib non-free) a cada linea
 
    1.2- Se realiza actualiza.
      $sudo apt-get update
      $sudo apt-get upgrade
  
    1.3- Instalamos MariaDB  
      $sudo apt-get install mariadb-server mariadb-client
 
    1.4- Conectamos con mysql
      $sudo mysql
 
    1.4.1- Se crea base de datos
      >create database tecnologia;
      >create database inventario;

    1.4.2- Se crea usuario
      >create user 'user01'@'%' identified by 'Infocom123456';

    1.4.3- Se dan privilegios a usuario.
      >grant all privileges on tecnologia.* to 'user01'@'%';

    1.4.4- Forzar actualizacion privilegios
      >flush privileges;

    1.4.5- Nos conectamos a la base de datos con el nuevo usuario, y se ingresa contrasena.
      >mysql -u user01 -p;

    1.4.6- Para usar una base de datos
      >use tecnologia;

      1.4.6.1- Se crea tabla 

      CREATE TABLE `users` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `fullname` varchar(150) DEFAULT NULL,
      `username` varchar(50) DEFAULT NULL,
      `password` varchar(50) DEFAULT NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

      1.4.6.2- Se ingresa registro en base de datos
      INSERT INTO `inventario`.`users`
      (`fullname`,
      `username`,
      `password`)
      VALUES
      ('Clark Kent',
      'ckent',
      'Superman');