# InfocomII

1- Se crean 3 maquinas virtuales en AZURE, 1 donde se encontrara la openvpn, otra para el servidor de base de datos y otra para el servidor web.
  1.1- Creacion de las 3 maquinas.

    1.1.1- Con Discos Standar y con acceso por ssh.

  ![Comando tree](Maquinas.png "Comando tree")

  ![Comando tree](Disco.png "Comando tree")

 
    1.1.2- Se crea red virtual ServerDebPII con la red 10.0.0.0/16 y subredes 10  .0.2.0/24, 10.0.7.0/25 y 10.0.8.0/24. Esto para que cada servidor este en una misma red pero en varias subredes.

  ![Comando tree](CrearRed.png "Comando tree")

  ![Comando tree](Red.png "Comando tree")

    1.1.3- En pestana etiquetas creamos las etiquetas que necesitamos; y en Revisar y crear, verificamos que la configuracion seleccionada es la correcta y seleccionamos crear.

    1.1.4- En maquina de la vpn abrimos los puertos 80 y 1194.

  ![Comando tree](Crear.png "Comando tree")

2- Se actualizan servidores
 $sudo apt-get update
 $sudo apt-get upgrade