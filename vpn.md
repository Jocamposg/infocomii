# InfocomII

1- Servidor con VPN
  1.1-  Se instala Apache2 para visulizar CRL, openvpn para la vpn y ufw como firewall.
    $sudo apt-get install apache2
    $sudo apt-get install ufw

  1.2- Se instala openvpn para vpn y esayrsa para generar certificados CA.
    
  1.2.1 - Se instala openvpn.
    $sudo apt install openvpn
   
  1.2.2- Para crear la infraestructura de CA y PKI se descraga se descomprime easyrsa.
    $wget -P ~/ https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.6/EasyRSA-unix-v3.0.6.tgz
    $cd ~
    $tar xvf EasyRSA-3.0.6.tgz
   
  1.2.3- Se configuran las variables de EasyRSA y se crea la CA.
  1.2.3.1- Nos dirigimos a directorio EasyRSA/ y hacemos copia del archivo vars.example como vars.
    $cd ~/EasyRSA-3.0.6/
    $cp vars.example vars
   
  1.2.3.2- Editamos archivo vars y establecemos valores para el nuevo certificado y quitamos comentarios eliminando el # y guardamos y salimos.
    $nano vars

    set_var EASYRSA_REQ_COUNTRY    "CR"
    set_var EASYRSA_REQ_PROVINCE   "Alajuela"
    set_var EASYRSA_REQ_CITY       "Quesada"
    set_var EASYRSA_REQ_ORG        "Mi Organizacion"
    set_var EASYRSA_REQ_EMAIL      "admin@infocom.isw612.xyz"
    set_var EASYRSA_REQ_OU         "Tecnologias"

  1.2.3.2- Ejecutamos la secuencia de comandos para iniciar infraestructura de llave publica en server. 
    $./easyrsa init-pki 
   
  1.2.3.3- Ejecutamos secuencia de comandos para crear archivos ca.crt y ca.key que representaran lado publico y privado del certificado SSL. En resultado se solicitara ingresar el nombre de su CA.
    $./easyrsa build-ca nopass

  1.2.3.4- Se crean los archivos de certificado, clave y cifrado del servidor.
  1.2.3.4.1- Ejecutamos secuencia de comandos para crear clave privada y archivo de solicitud de certificado llamado server.req. Y se copia el server.key en la carpeta openvpn.
    $./easyrsa gen-req server nopass
    $sudo cp private/server.key /etc/openvpn/

  1.2.3.5- Firmamos la solicitud con tipo de solicitud y nombre comun, confirmando con yes.
    $./easyrsa sign-req server server

  1.2.3.6- Se copia server.crt y ca.crt al directorio /etc/openvpn/.    
    $sudo cp pki/issued/server.crt  /etc/openvpn/
    $sudo cp pki/ca.crt  /etc/openvpn/

  1.2.3.7- Se crea una clave segura Diffie-Hellman para usarla durante el intercambio de claves.
    $./easyrsa gen-dh
   
  1.2.3.8- Se genera una firma HMAC para fortalecer las capacidades de verificación de integridad TLS.
    $sudo openvpn --genkey --secret ta.key
   
  1.2.3.9- Se copian los dos archivos nuevos en su /etc/openvpn/.
    $sudo cp ~/EasyRSA-v3.0.6/ta.key /etc/openvpn/
    $sudo cp ~/EasyRSA-v3.0.6/pki/dh.pem /etc/openvpn/

  1.2.3.10- Se genera un certificado de cliente y un par de claves.
  1.2.3.11- Se crea una estructura de directorios dentro de su directorio de inicio para almacenar los archivos de certificado y clave de cliente y se bloquean sus permisos. 
    $mkdir -p ~/client-configs/keys
    $chmod -R 700 ~/client-configs

  1.2.3.11- En directorio EasyRSA, ejecute la secuencia de comandos ​​​​​​easyrsa con las opciones gen-req y nopass, junto con el nombre común para el cliente.
    $./easyrsa gen-req client1 nopass
  
  1.2.3.12- Copie el archivo client1.key al directorio /client-configs/keys/.
    $sudo cp pki/private/client1.key ~/client-configs/keys/
   
  1.2.3.13- Firmamos la solicitud y confirmamos con yes.
    $./easyrsa sign-req client client1
   
  1.2.3.14- Se transfiera este archivo de vuelta al servidor.
    $sudo cp pki/issued/client1.crt ~/client-configs/keys/

  1.2.3.155- Se copia también los archivos ca.crt y ta.key al directorio /client-configs/keys/.
    $sudo cp ta.key ~/client-configs/keys/
    $sudo cp /etc/openvpn/ca.crt ~/client-configs/keys/
  
  1.2.4- Se configura el servicio de OpenVPN. 
  1.2.4.1- Se copia un archivo de configuración de OpenVPN de muestra al directorio de configuración y luego se extrae para usarlo como base para su configuración.
    $sudo cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/
    $sudo gzip -d /etc/openvpn/server.conf.gz 
  
  1.2.4.2- Se edita archivo server.conf.
    $sudo nano /etc/openvpn/server.conf
    Se verifican que no existan comentarios en linea.
    tls-auth ta.key 0 # This file is secret
    cipher AES-256-CBC
     
    Agregue una directiva auth para seleccionar el algoritmo de codificación de mensajes HMAC.
    auth SHA256
     
    Agregue rutas para los rangos de IP de los grupos de clientes.
    route 10.8.0.0 255.255.255.128
    route 10.8.8.128 255.255.255.128

    Debido a que asignaremos direcciones IP fijas para administradores y contratistas de sistemas específicos, utilizaremos un directorio de configuración del cliente.
    cliente-config-dir ccd  
    
    Encuentre la línea que contenga la directiva dh, si es necesario, cambie el nombre de archivo que aparece eliminando 2048 para que coincida con la clave que generó en el paso anterior.
    dh dh.pem
     
    Busque los ajustes user y group, y elimine comentarios en lineas.
    user nobody
    group nogroup

  1.2.4.3.- Se alican cambios de DNS, para dirigir todo trafico por la VPN en mismo archivo.
    Elimine comentario en seguiente lineas.
    push "redirect-gateway def1 bypass-dhcp"
    push "dhcp-option DNS 208.67.222.222"
    push "dhcp-option DNS 208.67.220.220"
    
    Se pueden modificar puerto y protocolo, por defecto esta en 1194/udp.
    port 1194
    proto udp
    
    En caso de modificar protocolo cambiar linea explicit-exit-notify 0 a explicit-exit-notify 1.

    Si anteriormente seleccionó un nombre distinto durante el comando ./build-key-server, modifique las líneas cert y key que ve para apuntar a los archivos .crt y .key adecuados.
     Pro defecto se encuentran:
      cert server.crt
      key server.key
   
  1.2.4.4- La linea “client-config-dir ccd” indica que los archivos de configuracion que se aplicaran a los clientes estan en directorio ccd, por lo cual deberemos crearlo.
    $sudo mkdir /etc/openvpn/ccd
  
  1.2.4.4.1- Creamos y editamos el archivo creado para el cliente 
    $sudo nano /etc/openvpn/ccd/cliente1
    En este definimos la ip para dicho cliente.
    ifconfig-push 10.8.0.5 10.8.0.6

  1.2.4.5- Ajuste la configuración de redes del servidor.
  1.2.4.5.1- Modifique archivo /etc/sysctl.conf.
    $sudo nano /etc/sysctl.conf
    Descomente linea 
    net.ipv4.ip_forward=1
   
  1.2.4.6- Para leer el archivo y modificar los valores de la sesión actual.
    $sudo sysctl -p

  1.2.4.7 Antes de abrir el archivo de configuración del firewall para agregar las reglas de enmascaramiento, primero debe encontrar la interfaz de red pública de su máquina con el siguiente comando.
    $ip route | grep default

  1.2.4.8- Abra el /etc/ufw/before.rules archivo para agregar la configuración.
    $sudo nano /etc/ufw/before.rules
    Agregue las siguientes lineas se establecerá la política predeterminada de la cadena POSTROUTING en la tabla nat y se enmascarará el tráfico que provenga de la VPN.
    
    # START OPENVPN RULES
    # NAT table rules
    *nat
    :POSTROUTING ACCEPT [0:0] 
    # Allow traffic from OpenVPN client to eth0 (change to the interface you discovered!)
    -A POSTROUTING -s 10.8.0.0/8 -o eth0 -j MASQUERADE
    COMMIT
    # END OPENVPN RULES
   
  1.2.4.9- Se debe indicar a UFW que permita también los paquetes reenviados de forma predeterminada. Para hacer esto, abra el archivo /etc/default/ufw.
    $sudo nano /etc/default/ufw
    Cambiar la directiva DEFAULT_FORWARD_POLICY.
    
    DEFAULT_FORWARD_POLICY="ACCEPT"
   
  1.2.4.10- Se ajusta el firewall para permitir el tráfico hacia OpenVPN. Si no cambió el puerto ni el protocolo en el archivo /etc/openvpn/server.conf, deberá abrir el tráfico UDP al puerto 1194. Si modificó el puerto o el protocolo, sustituya los valores y se grega puerto openSSH en caso de no haberlo agregado.
    $sudo ufw allow 1194/udp
    $sudo ufw allow OpenSSH

  1.2.4.11- Luego de agregar esas reglas, deshabilite y vuelva a habilitar UFW para reiniciarlo y cargue los cambios de todos los archivos.
    $sudo ufw disable
    $sudo ufw enable
  
  1.2.4.12- Se inicia y habilita el servicio de OpenVPN.
  1.2.4.12.1- Inicie el servidor de OpenVPN especificando el nombre de su archivo de configuración como una variable de instancia después del nombre del archivo de unidad de systemd.
    $sudo systemctl start openvpn@server
   
  1.2.4.12.2- Para ver estado del openVPN.
    $sudo systemctl status openvpn@server
   
  1.2.4.12.3- También puede controlar que la interfaz tun0 de OpenVPN esté disponible escribiendo lo siguiente.
    $ip addr show tun0

  1.2.4.12.4- Se habilita para que se cargue de manera automática en el inicio.
    $sudo systemctl enable openvpn@server
  
  1.2.5- Se crea la infraestructura de configuración de clientes.
  1.2.5.1- Se crea un archivo de configuración “de base” y luego una secuencia de comandos que le permitirá generar archivos de configuración, certificados y claves de clientes exclusivos según sea necesario
    $mkdir -p ~/client-configs/files

  1.2.5.2- Se copia un archivo de configuración de cliente de ejemplo al directorio client-configs para usarlo como su configuración de base.
    $cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf ~/client-configs/base.conf

  1.2.5.3- Edite el archivo.
    $nano ~/client-configs/base.conf
    Esto dirige al cliente a la dirección de su servidor de OpenVPN.
    remote your_server_ip 1194
    
    Se verifica que protocolo coincida.
    proto udp
    
    Se eliminan los comentarios de las directivas user y group.
    user nobody
    group nogroup
    
    Se eliminan los comentarios de estas directivas
    ca ca.crt
    cert client.crt
    key client.key
    tls-auth ta.key 1
    
    Se ajustan lineas como se establecio en el /etc/openvpn/server.conf.
    cipher AES-256-CBC
    auth SHA256
    
    Agregue la directiva.
    key-direction 1

    Se agregan algunas líneas no incluidas. Aunque puede incluir estas directivas en todos los archivos de configuración de clientes, solo debe habilitarlas para clientes Linux que incluyan un archivo /etc/openvpn/update-resolv-conf. Esta secuencia de comandos usa la utilidad resolvconf para actualizar la información de DNS para clientes Linux.
    #script-security 2
    #up /etc/openvpn/update-resolv-conf
    #down /etc/openvpn/update-resolv-conf

  1.2.5.4- Se crea una secuencia de comandos simple que compila la configuración de base con el certificado, la clave y los archivos de cifrado pertinentes, y luego ubique la configuración generada en el directorio ~/client-configs/files
  1.2.5.4.1- Se abre archivo make_config.sh para modificar.
    $nano ~/client-configs/make_config.sh
   
  1.2.5.4.2- Se crea el script.
    #!/bin/bash

    # First argument: Client identifier

    KEY_DIR=/home/sammy/client-configs/keys
    OUTPUT_DIR=/home/sammy/client-configs/files
    BASE_CONFIG=/home/sammy/client-configs/base.conf

    cat ${BASE_CONFIG} \
        <(echo -e '<ca>') \
        ${KEY_DIR}/ca.crt \
        <(echo -e '</ca>\n<cert>') \
        ${KEY_DIR}/${1}.crt \
        <(echo -e '</cert>\n<key>') \
        ${KEY_DIR}/${1}.key \
        <(echo -e '</key>\n<tls-auth>') \
        ${KEY_DIR}/ta.key \
        <(echo -e '</tls-auth>') \
        > ${OUTPUT_DIR}/${1}.ovpn
   
  1.2.5.4.3- Se debe asegurar de marcar este archivo como ejecutable
    $chmod 700 ~/client-configs/make_config.sh

  1.2.6- Se generan las configuraciones de clientes.
    1.2.6.1- Anteriormente se creó un certificado y una clave de cliente llamados client1.crt y client1.key. Se puede generar un archivo de configuración para estas credenciales en el directorio ~/client-configs y ejecutando secuencia de comandos.
      $cd ~/client-configs
      $sudo ./make_config.sh client1

    1.2.6.2- Se puede verificar archivo creado en files.
      $ls ~/client-configs/files

    1.2.6.3- Se debe transferir el archivo al dispositivo que necesita utilizar como cliente. Una forma es por sftp.
      $sftp user01@your_server_ip:client-configs/files/client1.ovpn ~/
   
  1.2.7- Instalacion de openvpn en dsipositivos finales.
  1.2.7.1- Instalación de la configuración del cliente en windows
    -Descargue la aplicación de cliente de OpenVPN para Windows de la página de descargas de OpenVPN. Seleccione la versión adecuada del instalador para su versión de Windows.
    -Luego de instalar OpenVPN, copie el archivo .ovpn a esta ubicación
      C:\Program Files\OpenVPN\config
    
    -Cuando inicie OpenVPN, este detectará el perfil de manera automática y lo dejará disponible.
    -Debe ejecutar OpenVPN como administrador cada vez que lo use, aun en cuentas administrativas. Para realizar esto sin tener que hacer clic con el botón secundario y seleccionar Ejecutar como administrador cada vez que use la VPN, debe fijarlo como ajuste predeterminado desde una cuenta administrativa.
    -Una vez que se inicie OpenVPN, establezca una conexión ingresando al área de notificación y haga clic con el botón secundario en el ícono de OpenVPN. Con esto, se abrirá el menú contextual. Seleccione** client1** en la parte superior del menú (su perfil client1.ovpn) y luego** Connect**.
    -Desconéctese de la VPN de la misma forma: ingrese al applet de la bandeja del sistema, haga clic con el botón secundario en el ícono de OpenVPN, seleccione el perfil del cliente y haga clic en ****Disconnect.


  1.2.7.2- Instalación de la configuración del cliente en debian.
    -Puede instalarlo como en el servidor escribiendo lo siguiente
      $sudo apt update
      $sudo apt install openvpn

    -Verifique si su distribución incluye una secuencia de comandos /etc/openvpn/update-resolv-conf.
      $ls /etc/openvpn

    -Edite el archivo de configuración de cliente de OpenVPN que transfirió.
      $nano client1.ovpn

    -Elimine los comentarios de las tres líneas de agregó para modificar los ajustes de DNS.
      script-security 2
      up /etc/openvpn/update-resolv-conf
      down /etc/openvpn/update-resolv-conf

    -Ahora, podrá conectarse a la VPN simplemente apuntando el comando openvpn hacia el archivo de configuración de cliente.
      $sudo openvpn --config client1.ovpn

  1.2.7.3- Instalación de la configuración del cliente en android.
    -Abra Google Play Store. Busque e instale Android OpenVPN Connect
    -Puede transferir el perfil .ovpn conectando el dispositivo Android a su computadora a través de un puerto USB y copiando el archivo.
    -Inicie la aplicación OpenVPN y haga clic en el menú para importar el perfil.
    -Luego, diríjase a la ubicación del perfil guardado y seleccione el archivo. La aplicación notificará que se importó el perfil.
    -Para conectarse, simplemente toque el botón Connect.


  1.3-Servidor con DNS.
 
  1.3.1- Instalamos los paquetes requeridos.
    $sudo apt-get install -y bind9 bind9utils bind9-doc dnsutils
  
  1.3.1.1- Creamos una zonas de reenvío y reverso para el dominio en archivo /etc/bind/named.conf.
    $sudo nano /etc/bind/named.conf.local
    
    zone "infocomisw612" {
    type master;
    file "/etc/bind/forward.infocomisw612.db";
    allow-update { none; };
    };

    zone "8.0.10.in-addr.arpa" {
    type master;
    file "/etc/bind/reverse.infocomisw612.db";
    allow-update { none; };
    };
   
  1.3.1.2- Se copia archivo de ejemplo para crear un archivo a nuevo para el forward.
    $sudo cp /etc/bind/db.local /etc/bind/forward.infocomisw612.db

  1.3.1.3- Se edita archivo creado.
    $sudo nano /etc/bind/forward.infocomisw612.db
     ;
     ; BIND data file for infocomisw612
     ;
     $TTL    604800
     @       IN      SOA      ns1.infocomisw612. info.infocomisw612. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
     ;
     @       IN      NS      ns1.infocomisw612.

     ns1       IN      A       10.0.8.4

     publico       IN      A       10.0.8.4
     privado       IN      A       10.0.8.4

  1.3.1.4- De igual manera copiamos un archivo reverse de ejemplo para crear uno nuevo.  
    $sudo cp /etc/bind/db.127 /etc/bind/reverse.infocomisw612.db
   
  1.3.1.5- Se modifica archivo.
    $sudo nano /etc/bind/reverse.infocomisw612.db
     ;
     ; BIND reverse data file for infocomisw612
     ;
     $TTL    604800
     @       IN      SOA     ns1.infocomisw612. info.infocomisw612. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
     ;
     @       IN      NS      ns1.infocomisw612.

    4   IN      PTR     ns1.infocomisw612.

    4   IN      PTR     publico.infocomisw612.
    4   IN      PTR     privado.infocomisw612.

  1.3.1.6- Reiniciamos bind9.
    $sudo systemctl restart bind9

  1.3.1.7- Para verificar el dns podemos modificar el archivo resolve.conf
    $sudo nano /etc/resolv.conf
    Le ingresamos la ip de nuestro servidor dns.
    nameserver 192.168.0.10

  1.3.1.8- Usamos el comando dig para ver el forward lookup y el reverse lookup.
    $dig @10.0.2.4 publico.infocomisw612
    ![Comando tree](Forward.png "Comando tree")
    
    $dig -x @10.0.2.4 10.0.8.4




