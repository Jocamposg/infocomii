# InfocomII

1- Servidor Web
  1.1- Se instala Apache2, para visulizar paginas web.
    $sudo apt-get install apache2 

  1.2- Crearemos el directorio donde se alojarán los archivos web del servidor virtual.
    $mkdir -p public_html/infocomisw612
  
  1.3- Vamos a incluir un contenido mínimo de prueba, creando un archivo public_html/dominio.local/index.html y le ingresamos.
    $nano public_html/infocomisw612/index.html
 
  1.4 - Ahora, con privilegios de sudo, creamos el archivo de configuración del servidor virtual.
    $sudo nano /etc/apache2/sites-available/infocomisw612.conf
    
    <VirtualHost *:80>
        ServerName infocomisw612
        ServerAlias publico.infocomisw612
        DocumentRoot /home/user01/public_html/infocomisw612
        <Directory /home/user01/public_html/infocomisw612>
                Options -Indexes
                AllowOverride All
                Require all granted
        </Directory>
        LogLevel info
        ErrorLog ${APACHE_LOG_DIR}/infocomisw612-error.log
        CustomLog ${APACHE_LOG_DIR}/infocomisw612-access.log combined
    </VirtualHost>

  1.5 - Ahora, con privilegios de sudo, modificamos el html por defecto del apache para el otro host.
    $sudo nano /var/www/html/index.html
    
    <VirtualHost *:80>
        ServerName infocomisw612
        ServerAlias privado.infocomisw612
        DocumentRoot /var/www/html
        <Directory /var/www/html>
                Options -Indexes
                AllowOverride All
                Require all granted
        </Directory>
        LogLevel info
        ErrorLog ${APACHE_LOG_DIR}/infocomisw612-error.log
        CustomLog ${APACHE_LOG_DIR}/infocomisw612-access.log combined
    </VirtualHost>

  1.6- Para que funcione un servidor virtual cuyo archivo de configuración acabamos de crear, es necesario activarlo con el comando a2ensite.
    $sudo a2ensite infocomisw612
  
  1.7- Como nos indica la salida del comando, hay que recargar la configuración de Apache con systemctl.
    $sudo systemctl reload apache2